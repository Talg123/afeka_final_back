module.exports = (sequelize, Sequelize) => {
    const Clerks = sequelize.define("clerks", {
        userID: {
            type: Sequelize.INTEGER,
            primaryKey: true,
        },
        fullName: {
            type: Sequelize.STRING,
        },
        email: {
            type: Sequelize.STRING,
            validate:{
                isEmail: true,
            },
            allowNull: true
        },
        password: {
            type: Sequelize.STRING
        },
        permissionLevel: {
            type: Sequelize.INTEGER,
            defaultValue: 0
        }
    });

    return Clerks;
};