const db = require("../models");
const Clerk = db.clerks;
const Op = db.Sequelize.Op;


// Create and Save a new Tutorial
exports.create = async (req, res) => {
    if (!req.body.fullName || !req.body.password) {
        res.status(400).send({
            msg: "Content must include userID, fullName and password"
        });
        return;
    }
    const { userID, fullName, email, password, permissionLevel } = req.body
    const clerk = {
        userID: userID,
        fullName: fullName,
        email: email,
        password: password,
        permissionLevel: permissionLevel
    }
    const ans = await Clerk.create(clerk).catch(err => errHandler(err, res))
    res.send(ans);
};

exports.findAll = async (req, res) => {
    const fullName = req.query.fullName;
    const condition = fullName ? { fullName: { [Op.like]: `%{fullName}` } } : null;

    const ans = await Clerk.findAll({ where: condition }).catch(err => errHandler(err, res))
    res.send(ans);
};

exports.findOne = async (req, res) => {
    const userID = req.params.userID;
    const ans = await Clerk.findByPk(userID).catch(err => errHandler(err, res))
    res.send(ans);
};

exports.update = async (req, res) => {
    debugger;
    const userID = req.params.userID;
    const ans = await Clerk.update(req.body, {
        where: { userID: userID }
    }).catch(err => { errHandler(err, res); return; });
    returnMessage(res, ans, "updated");
};

exports.delete = async (req, res) => {
    const userID = req.params.userID;
    const ans = await Clerk.destroy({ where: { userID: userID } })
        .catch(err => { errHandler(err, res); return; });

    returnMessage(res, ans, "deleted");
};

exports.deleteAll = async (req, res) => {
    const ans = await Clerk.destroy({
        where: {},
        truncate: false
    }).catch(err => { errHandler(err, res); return; });
    returnMessage(res, ans, "", "All Clerks were deleted !");

};
const returnMessage = function (res, ans = "undefined", action = "action", difMessage = "undefined") {
    if (ans == "undefined") return;
    if (ans == 1) {
        res.send({ msg: `Clerk(s) ${action} successfully !` })
        return;
    }
    if (difMessage !== "undefined") {
        res.send({ message: difMessage });
        return;
    }
    res.send({ message: `Cannot ${action} Clerk with id=${userID}. Maybe Tutorial was not found` })
}
const errHandler = function (err, res) {
    res.status(500).send({
        msg: err.message || 'error in clerk action !'
    })
}