module.exports = {
    HOST: "localhost",
    USER: "afeka",
    PASSWORD: "123456",
    DB: "afekadb",
    dialect: "postgres",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  };