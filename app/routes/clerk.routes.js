module.exports = app =>{
    const clerks = require("../controllers/clerks.controller.js")
    const router = require("express").Router();

    router.post("/", clerks.create);
    router.get("/", clerks.findAll);
    router.get("/:userID", clerks.findOne);
    router.put("/:userID", clerks.update);
    router.delete("/:userID", clerks.delete);
    router.delete("/", clerks.deleteAll);
    app.use('/api/clerks', router);
}