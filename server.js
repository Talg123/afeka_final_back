const express = require("express");
const bosyParser = require("body-parser");
const cors = require("cors");
const db = require("./app/models");

const app = express();

var corsOptions = {
    origin: "http://localhost:8081"
}

app.use(bosyParser.json());
app.use(bosyParser.urlencoded({ extended: true }));

db.sequelize.sync({ force: true }).then(() => {
    console.log("Drop and re-sync db.");
});

app.get("/", (req, res) => {
    res.json({ msg: "welcome to app" });
});
require("./app/routes/clerk.routes")(app);
const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
    console.log(`Server runs on port ${PORT}.`);
});

